FROM php:8.3-cli

MAINTAINER v.nivuahc <vni.dev@zaclys.net>

RUN apt-get update > /dev/null 2>&1 && \
    apt-get -y install \
        curl \
        git \
        zip \
        unzip > /dev/null 2>&1 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* > /dev/null 2>&1
    
RUN docker-php-ext-install bcmath

RUN curl --silent https://getcomposer.org/installer | php > /dev/null 2>&1 && \
    mv ./composer.phar /usr/local/bin/composer > /dev/null 2>&1

RUN composer global require \
    phpunit/phpunit \
    squizlabs/php_codesniffer \
    friendsofphp/php-cs-fixer \
    phpmd/phpmd \
    behat/behat \
    phploc/phploc > /dev/null 2>&1

RUN ln -s /root/.composer/vendor/bin/* /usr/local/bin/ > /dev/null 2>&1

