PHP Toolbox
===========

Ces deux fichiers permettent de créer et d'utiliser un conteneur docker contenant **php-cli** 
et tous les outils utiles pour le développement PHP :

* composer
* phpunit/phpunit
* squizlabs/php_codesniffer
* friendsofphp/php-cs-fixer
* phpmd/phpmd
* behat/behat
* phploc/phploc

Les images disponibles sont visibles ici : 
[https://hub.docker.com/r/vnivuahc/php-tools](https://hub.docker.com/r/vnivuahc/php-tools).  
Elles fonctionnent exactement comme celles proposées ici :
[https://hub.docker.com/r/devdrops/php-toolbox](https://hub.docker.com/r/devdrops/php-toolbox)


## Objectifs ##

L'intérêt est d'éviter d'installer une version de PHP spécifique sur votre poste, et
d'utiliser un conteneur docker à la place.

Le but est de pouvoir lancer une commande PHP en tapant simplement :

```bash
make run COMMAND='ma commande par exemple php -v'
```

ou mieux

```bash
mr 'php -v'
```

## Utilisation ##

Pour une utilisation normale, seul le fichier `Makefile` vous est nécessaire.

### Makefile ###

Ce fichier est configuré pour utiliser **PHP 8.3**.  
Vous pouvez le modifier et remplacer `latest` par le tag que vous souhaitez.

Les numéros de tags sont ceux des images docker disponibles (cf. plus haut).  
Le numéro du tag correspond à la version de PHP-cli qu'il contient.

Par exemple, pour utiliser **PHP 5.6**, remplacez `latest` par `5.6`.

### Exécuter une commande ###

Grâce au `Makefile` vous pouvez lancer rapidement une commande via : 

```bash
make run COMMAND='ma commande'
```

Par exemple : 

```bash
make run COMMAND='php -v'
make run COMMAND='composer install'
make run COMMAND='phpunit --version'
make run COMMAND='phpcs --standard=PSR2 Abstract.php'
make run COMMAND='php-cs-fixer fix Abstract.php'
make run COMMAND='phpmd Abstract.php text codesize'
make run COMMAND='behat --version'
make run COMMAND='phploc --version'
```

Le Makefile est paramétré pour exécuter vos commandes dans le répertoire courant.

**Il faut toutefois que le fichier `Makefile` s'y trouve !**

## Alias système ##

Pour raccourcir encore les appels à `make`, vous pouvez ajouter cette fonction 
à votre fichier `~/.bashrc` (ou `~/.bash_aliases`, ou autre si vous n'êtes pas sous Ubuntu) :

```
mr() { make Makefile run COMMAND="$1"; }
```

Vous pourrez désormais lancer vos commandes ainsi :

```bash
mr 'ma commande'
```

**Bonus :**

Si vous ne souhaitez pas que le fichier `Makefile` soit à la racine de votre projet, 
vous pouvez en profiter pour améliorer la fonction.  
Imaginons que voulez le placer dans un répertoire `bin/` à la racine de votre projet :

```
mr() { make -f ./bin/Makefile run COMMAND="$1"; }
```

**Attention :** Vous devrez alors modifier votre `Makefile` pour qu'il lance son exécution
en dehors de `bin/`. Ajoutez `..` après `$(PWD)` :

```
-v $(PWD)..:/toolbox \
```

## Créer une nouvelle image ##

Si vous souhaitez utiliser une image avec une autre version de PHP et que l'image docker 
correspondante n'est pas disponible, vous pouvez la construire à partir du `Dockerfile`.

Il sera ensuite peut-être nécessaire de l'envoyer sur un dépôt docker 
(ex : [https://hub.docker.com](https://hub.docker.com))

Pour créer une nouvelle image : 

* Modifiez la première ligne du fichier `Dockerfile` et indiquez l'image de PHP-cli à 
utiliser. 
Par exemple `FROM php:5.6-cli`, pour utiliser une image contenant `PHP5.6-cli`.

* Lancez maintenant la commande de build :

```bash
docker build -t mon/image:5.6 - < Dockerfile
```

L'image doit maintenant être visible dans le dépôt local : 

```bash
docker images
```

* Pour l'envoyer sur votre dépôt : 

```bash
docker login
docker push mon/image
```

**Remarque** :

Pour utiliser un dépôt spécifique (ex : `registry.gitlab.com`), il faut en préfixer le nom de l'image, pour le build et 
pour le push, accompagné du nom d'utilisateur (ex : `registry.gitlab.com/mon-nom-utiliateur/mon/image`).
